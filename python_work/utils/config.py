'''
Author          : Bijan R.Rofoee
Version         : 0.1
'''

import yaml

config_file = "utils/config.yml"


class Config():
    def __init__(self):
        data = read_config()


def read_config():
    with open(config_file) as conf:
        try:
            return yaml.load(conf)
        except yaml.YAMLError as e:
            exit(1)


if __name__ == "__main__":
    con = Config()
