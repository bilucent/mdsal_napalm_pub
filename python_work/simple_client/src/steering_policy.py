'''
Author          : Bijan R.Rofoee
'''
import logging
import json
import requests
import time


class ODLSteering():
    """

    """
    def __init__(self, conf):
        self.odl_ip = conf["ODL_IP"]
        self.odl_port = conf["ODL_port"]
        self.policy_node = conf["node1"]["name"]
        self.monitor_node = conf["node2"]["name"]
        self.odl_user = conf["ODL_user"]
        self.odl_pass = conf["ODL_pass"]
        self.req_hdrs = {'Content-Type': 'application/json'}
        self.interface1 = conf["intf1"]
        self.interface2 = conf["intf2"]
        self.test_odl()


    def right_left_op(self, policy_name, policy_action):
        '''
        this URL is available after building the ODL project and installing RESTCONF
        '''
        url = 'http://' + self.odl_ip + ':' + self.odl_port + '/restconf/operations/confy:leftright-policy-intf-oper'
        request_template = """
        {
            "input": {
                "policy-name" : "%s",
                "leftright": "%s"
            }
        }
        """
        request_template_old = """
        {
            "input": {
                "mount-name": "%s",
                "policy-name" : "%s",
                "leftright": "%s",
                "ipv4-prefix" : "55.55.55.55",
                "ipv4-prefix2" : "55.55.55.55",
                "next-hop-intf" : "tunnel-te11",
                "next-hop-intf2" : "tunnel-te12",        
                "next-hop-metric" : "%s",
                "next-hop-metric2" : "%s"                  
            }
        }
        """

        req_body = request_template % (policy_name, policy_action)
        try:
            resp = requests.post(url, data=req_body, headers=self.req_hdrs, auth=(self.odl_user, self.odl_pass))
            return resp
        except:
            logging.error("request failed")
            exit(1)

    def right_left_design(self, policy_name, policy_action, metric, metric2):
        url = 'http://' + self.odl_ip + ':' + self.odl_port + '/restconf/operations/confy:leftright-policy-intf-design'
        request_template = """
        {
            "input": {
                "mount-name": "%s",
                "ipv4-prefix" : "55.55.55.55",
                "ipv4-prefix2" : "55.55.55.55",
                "next-hop-intf" : "tunnel-te11",
                "next-hop-intf2" : "tunnel-te12",        
                "next-hop-metric" : "%s",
                "next-hop-metric2" : "%s"                  
            }
        }
        """

        req_body = request_template % (self.policy_node, policy_name, policy_action, metric, metric2)

        try:
            resp = requests.post(url, data=req_body, headers=self.req_hdrs, auth=(self.odl_user, self.odl_pass))

            return resp
        except:
            logging.error("request failed")
            exit(1)

    # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    # the three functions below demonstrate how different services are decoupled.
    # the logic basically is a substraction.
    # then there is the funtion which fetched the utilisation per interface.
    def get_util(self):
        url = 'http://' + self.odl_ip + ':' + self.odl_port + '/restconf/operations/confy:interface-util'
        request_template = """
        { 
            "input": {    
                "node-name": %s,    
                "interface-name": "eth0"    
            }
        }
        """
        req_body = request_template % (self.monitor_node)
        try:
            resp = requests.post(url, data=req_body, headers=self.req_hdrs, auth=(self.odl_user, self.odl_pass))
            return resp.content
        except Exception:
            logging.error("request call failed")

    def simple_rate_cal(self, interface):
        data1 = float(self.simple_fetch_util(interface))
        time.sleep(1)  # delays for 5 seconds
        data2 = float(self.simple_fetch_util(interface))
        return data2 - data1

    # interface util fetcher
    def simple_fetch_util(self, interface):
        resp = self.get_util()
        results = json.loads(resp)
        return self.json_parse_util(results, interface)

    # interface util parser to get the right information for specifc interface
    def json_parse_util(self, results, interface):
        for o in results["output"]["ifo"]:
            ifos = o
            if ifos['interface-name'] == interface:
                return ifos['total-frames-transmitted']

    def test_odl(self):
        req_hdrs = {'Content-Type': 'application/xml'}

        url = 'http://' + self.odl_ip + ':' + self.odl_port + '/restconf/modules'
        try:
            resp = requests.get(url, headers=req_hdrs, auth=(self.odl_user, self.odl_pass))
            return resp.content, resp.status_code
        except:
            print("ODL not connected")
            exit(1)

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    # some random numbers used for showing how decision thresholds work
    def te_logic(self, rate):
        print("rate ", rate)
        if rate <= 15:
            return "LEFT", 10, 0
        elif rate > 15 and rate <= 25:
            return "LB", 10, 10
        elif rate > 25:
            return "LB", 10, 20
        else:
            pass
