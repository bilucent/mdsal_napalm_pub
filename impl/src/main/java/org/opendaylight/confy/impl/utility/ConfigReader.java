package org.opendaylight.confy.impl.utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ConfigReader {

		public static void main(String args[]) throws FileNotFoundException {
			ConfigReader conf = new ConfigReader();
			Map<String, String> nameip_map = conf.config_reader();
			System.out.println(nameip_map.keySet());
			System.out.println(nameip_map.values());
			System.out.println(nameip_map.get("node3"));

		}

		public Map<String, String> config_reader() throws FileNotFoundException {


			File pathfile = new File("pathfile.txt");
			String path = pathfile.getPath();
			System.out.println("path is : "+ path);


			Map<String,String> nameip_map = new HashMap<String,String>();
			File file = new File("./etc/config.yaml");
			Scanner sc = new Scanner(file);
			sc.useDelimiter("\\Z");
//			Scanner config = sc.useDelimiter("\\Z");

			String tmp = sc.next();
			String[] tmp_array =tmp.split("\n");

			for (int i =0;  i < tmp_array.length; i++){
				nameip_map.put(tmp_array[i].split(":")[0],tmp_array[i].split(":")[1]);

			}

			System.out.println(nameip_map);
			return nameip_map;
		}
}
