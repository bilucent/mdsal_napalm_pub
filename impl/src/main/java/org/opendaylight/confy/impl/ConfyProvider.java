/*
 * Copyright © 2017 Yoyodyne, Inc. and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package org.opendaylight.confy.impl;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.CheckedFuture;
import com.google.common.util.concurrent.Futures;
import org.opendaylight.confy.impl.utility.ConfigReader;
import org.opendaylight.controller.md.sal.binding.api.*;
import org.opendaylight.controller.md.sal.binding.api.DataChangeListener;
import org.opendaylight.controller.md.sal.common.api.data.*;
import org.opendaylight.controller.sal.binding.api.BindingAwareBroker;
import org.opendaylight.controller.sal.binding.api.RpcConsumerRegistry;
import org.opendaylight.controller.sal.binding.api.RpcProviderRegistry;


import org.opendaylight.confy.impl.listener.LoggingNotificationListener;
import org.opendaylight.confy.impl.listener.PerformanceAwareNotificationListener;
//import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.confy.rev150105.
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.drivers.media.eth.oper.rev151014.ethernet._interface.statistics.Statistic;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.drivers.media.eth.oper.rev151014.ethernet._interface.Statistics;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.drivers.media.eth.oper.rev151014.EthernetInterface;

import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.address.family.AddressFamily;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.address.family.address.family.Vrfipv4;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.router._static.DefaultVrf;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.vrf.next.hop.VrfNextHopInterfaceNameBuilder;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.vrf.unicast.VrfUnicast;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.xr.types.rev150629.InterfaceName;
import org.opendaylight.yang.gen.v1.org.opendaylight.confy.example.notifications.rev150611.ExampleNotificationsListener;
import org.opendaylight.yang.gen.v1.org.opendaylight.confy.example.notifications.rev150611.VrfRouteNotification;
import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.netconf.notification._1._0.rev080714.CreateSubscriptionInputBuilder;
import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.netconf.notification._1._0.rev080714.NotificationsService;
import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.netconf.notification._1._0.rev080714.StreamNameType;
import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.yang.ietf.inet.types.rev130715.Ipv4AddressNoZone;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.confy.rev150105._interface.util.output.Ifo;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.confy.rev150105._interface.util.output.IfoBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.confy.rev150105.delete.routes.input.RouteList;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.confy.rev150105.show.node.output.IfCfgDataBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.confy.rev150105.show.node.output._if.cfg.data.Ifc;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.confy.rev150105.show.node.output._if.cfg.data.IfcBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.confy.rev150105.show.node.output._if.cfg.data.IfcKey;

import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ifmgr.cfg.rev150730.InterfaceConfigurations;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ifmgr.cfg.rev150730._interface.configurations.InterfaceConfiguration;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ifmgr.oper.rev150730.InterfaceProperties;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ifmgr.oper.rev150730._interface.properties.DataNodes;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ifmgr.oper.rev150730._interface.properties.data.nodes.DataNode;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ifmgr.oper.rev150730._interface.properties.data.nodes.data.node.Locationviews;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ifmgr.oper.rev150730._interface.properties.data.nodes.data.node.locationviews.Locationview;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ifmgr.oper.rev150730._interface.table.Interfaces;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ifmgr.oper.rev150730._interface.table.interfaces.Interface;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.RouterStatic;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.address.family.AddressFamilyBuilder;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.address.family.address.family.Vrfipv4Builder;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.router._static.Vrfs;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.router._static.vrfs.Vrf;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.router._static.vrfs.VrfBuilder;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.router._static.vrfs.VrfKey;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.vrf.next.hop.VrfNextHopNextHopAddressBuilder;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.vrf.prefix.table.VrfPrefixes;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.vrf.prefix.table.VrfPrefixesBuilder;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.vrf.prefix.table.vrf.prefixes.VrfPrefix;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.vrf.prefix.table.vrf.prefixes.VrfPrefixBuilder;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.vrf.prefix.table.vrf.prefixes.VrfPrefixKey;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.vrf.route.VrfRouteBuilder;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.vrf.route.vrf.route.VrfNextHopTableBuilder;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.ip._static.cfg.rev150910.vrf.unicast.VrfUnicastBuilder;

import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.policy.repository.cfg.rev150827.RoutingPolicy;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.policy.repository.cfg.rev150827.routing.policy.RoutePolicies;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.policy.repository.cfg.rev150827.routing.policy.route.policies.RoutePolicy;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.policy.repository.cfg.rev150827.routing.policy.route.policies.RoutePolicyKey;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.ios.xr.policy.repository.cfg.rev150827.routing.policy.route.policies.RoutePolicyBuilder;
import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.xr.types.rev150629.CiscoIosXrString;

import org.opendaylight.yang.gen.v1.http.cisco.com.ns.yang.cisco.xr.types.rev150629.RplPolicy;
//import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.yang.ietf.inet.types.rev130715.IpAddress;
import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.yang.ietf.inet.types.rev130715.IpAddressNoZone;
//import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.yang.ietf.inet.types.rev130715.Ipv4AddressNoZone;
import org.opendaylight.yang.gen.v1.urn.opendaylight.netconf.node.topology.rev150114.NetconfNode;
import org.opendaylight.yang.gen.v1.urn.opendaylight.netconf.node.topology.rev150114.network.topology.topology.topology.types.TopologyNetconf;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.confy.rev150105.*;

import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.confy.rev150105.write.routes.input.Route;

import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.NetworkTopology;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.NodeId;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.TopologyId;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.Topology;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.TopologyKey;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.topology.Node;

import org.opendaylight.yang.gen.v1.urn.opendaylight.netconf.node.topology.rev150114.NetconfNodeConnectionStatus;

import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.topology.NodeKey;
import org.opendaylight.yangtools.concepts.ListenerRegistration;
import org.opendaylight.yangtools.yang.binding.*;
import org.opendaylight.yangtools.yang.common.QName;
import org.opendaylight.yangtools.yang.common.RpcResult;
import org.opendaylight.yangtools.yang.common.RpcResultBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ConfyProvider  implements DataChangeListener, ConfyService,
            AutoCloseable {

        private static final Logger LOG = LoggerFactory.getLogger(ConfyProvider.class.getName());

        public static final InstanceIdentifier<Topology> NETCONF_TOPO_IID =
                InstanceIdentifier
                        .create(NetworkTopology.class)
                        .child(Topology.class,
                                new TopologyKey(new TopologyId(TopologyNetconf.QNAME.getLocalName())));


        //        get the node and IP information from the config.yml file

        ConfigReader conf = new ConfigReader();
        Map<String, String> nameip_map = conf.config_reader();
        //        System.out.println(nameip_map.keySet());
        //        System.out.println(nameip_map.values());
        String node1 = nameip_map.get("node1");
        String node2 = nameip_map.get("node2");
        String node3 = nameip_map.get("node3");
        String node4 = nameip_map.get("node4");
        String node5 = nameip_map.get("node5");

        private BindingAwareBroker.RpcRegistration<ConfyService> rpcReg;
        private ListenerRegistration<DataChangeListener> dclReg;
        private MountPointService mountPointService;
        private DataBroker dataBroker;
        private RpcResult<Void> SUCCESS = RpcResultBuilder.<Void>success().build();

        private final RpcProviderRegistry rpcProviderRegistry;

        public ConfyProvider(DataBroker dataBroker,
                                   RpcProviderRegistry rpcProviderRegistry,
                                   MountPointService mountPointService) throws FileNotFoundException {
            this.dataBroker = dataBroker;
            this.rpcProviderRegistry = rpcProviderRegistry;
            this.mountPointService = mountPointService;


        }
        /**
         * initiate the service.
         */
        public void init() {

            LOG.info("Before Hello_boronProvider Session Initiated");
            rpcReg = rpcProviderRegistry.addRpcImplementation(ConfyService.class, this);
            LOG.info("After Hello_boronProvider Session Initiated");

        }
        /**
         * A method called when the session to MD-SAL is closed. It closes
         * registrations in MD-SAL
         * Listener with the MD-SAL
         */
        @Override
        public void close() throws Exception {
            LOG.info("ConfyProvider Closed");
            // Clean up the RPC service registration
            if (rpcReg != null) {
                rpcReg.close();
            }
            // Clean up the Data Change Listener registration
            if (dclReg != null) {
                dclReg.close();
            }

        }


    public void say_hi() throws InterruptedException {
        while(1==1)
        {
            System.out.println("aloha");
            TimeUnit.SECONDS.sleep(1);

        }
    }



    public Future<RpcResult<Void>> writeRoutes(WriteRoutesInput input) {
        final Optional<MountPoint> mountPoint;
        try {
            // Get mount point for specified device
            mountPoint = mountPointService.getMountPoint(mountIds.get(input.getMountName()));
        } catch (ExecutionException e) {
            throw new IllegalArgumentException(e);
        }

        // Get DataBroker API and create a write tx
        final DataBroker dataBroker = mountPoint.get().getService(DataBroker.class).get();
        final WriteTransaction writeTransaction = dataBroker.newWriteOnlyTransaction();

        // Build InstanceIdentifier to point to a specific VRF in the device
        final CiscoIosXrString name = new CiscoIosXrString(input.getVrfId());
        final InstanceIdentifier<Vrf> routesInstanceId;
        routesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                .child(Vrfs.class).child(Vrf.class, new VrfKey(name));

        // Prepare the actual routes for VRF. Transform Rpc input routes into VRF routes
        final VrfPrefixes transformedRoutes = new VrfPrefixesBuilder()
                .setVrfPrefix(Lists.transform(input.getRoute(), new Function<Route, VrfPrefix>() {

                    @Override
                    public VrfPrefix apply(final Route input) {
                        final IpAddressNoZone prefix = new IpAddressNoZone(input.getIpv4Prefix());
                        final IpAddressNoZone nextHop = new IpAddressNoZone(input.getIpv4NextHop());
                        final long prefixLength = input.getIpv4PrefixLength();
                        return new VrfPrefixBuilder()
                                .setVrfRoute(new VrfRouteBuilder()
                                        .setVrfNextHopTable(new VrfNextHopTableBuilder()
                                                .setVrfNextHopNextHopAddress
                                                        (Collections.singletonList
                                                                (new VrfNextHopNextHopAddressBuilder()
                                                                        .setNextHopAddress(nextHop)
                                                                        .build()))
                                                .build())
                                        .build())
                                .setPrefix(prefix)
                                .setPrefixLength(prefixLength)
                                .setKey(new VrfPrefixKey(prefix, prefixLength))
                                .build();

                    }
                })).build();

        // Build the parent data structure for VRF
        final Vrf newRoutes = new VrfBuilder()
                .setVrfName(name)
                .setKey(new VrfKey(name))
                .setAddressFamily(new AddressFamilyBuilder()
                        .setVrfipv4(new Vrfipv4Builder()
                                .setVrfUnicast(new VrfUnicastBuilder()
                                        .setVrfPrefixes(transformedRoutes)
                                        .build())
                                .build())
                        .build())
                .build();

        // invoke edit-config, merge the route list for vrf identified by input.getVrfId()
        writeTransaction.merge(LogicalDatastoreType.CONFIGURATION, routesInstanceId, newRoutes);

        // commit
        final CheckedFuture<Void, TransactionCommitFailedException> submit = writeTransaction.submit();
        return Futures.transform(submit, new Function<Void, RpcResult<Void>>() {
            @Override
            public RpcResult<Void> apply(final Void result) {
                LOG.info("{} Route(s) written to {}", input.getRoute().size(), input.getMountName());
                return SUCCESS;
            }
        });
    }


    @Override
    public Future<RpcResult<Void>> deleteRoutes(DeleteRoutesInput input) {
        final Optional<MountPoint> mountPoint;
        try {
            // Get mount point for specified device
            mountPoint = mountPointService.getMountPoint(mountIds.get(input.getMountName()));
        } catch (ExecutionException e) {
            throw new IllegalArgumentException(e);
        }

        // Get DataBroker API and create a write tx
        final DataBroker dataBroker = mountPoint.get().getService(DataBroker.class).get();
        final WriteTransaction writeTransaction = dataBroker.newWriteOnlyTransaction();

        // Build InstanceIdentifier to point to a specific VRF in the device
        final CiscoIosXrString name = new CiscoIosXrString(input.getVrfId());
        final InstanceIdentifier<Vrf> routesInstanceId;
        routesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                .child(Vrfs.class).child(Vrf.class, new VrfKey(name));

        final InstanceIdentifier<VrfPrefix> VProutesInstanceId;


        LOG.info("nput.getRouteList().get(0).getIpv4NextHop(); bijan:????", input.getRouteList().get(0).getIpv4NextHop());

        List<RouteList> rl = input.getRouteList();

        for (int i =0;i < rl.size(); i++)
        {
            LOG.info("l.listIterator().toString(); bijan:????", rl.listIterator().toString());
        }
        // Prepare the actual routes for VRF. Transform Rpc input routes into VRF routes

        final VrfPrefixes transformedRoutes = new VrfPrefixesBuilder()
                .setVrfPrefix(Lists.transform(input.getRouteList(), new Function<RouteList, VrfPrefix>() {

                    @Override
                    public VrfPrefix apply(final RouteList input) {
                        final IpAddressNoZone prefix = new IpAddressNoZone(input.getIpv4Prefix());
                        final IpAddressNoZone nextHop = new IpAddressNoZone(input.getIpv4NextHop());
                        final long prefixLength = input.getIpv4PrefixLength();
                        return new VrfPrefixBuilder()
                                .setVrfRoute(new VrfRouteBuilder()
                                        .setVrfNextHopTable(new VrfNextHopTableBuilder()
                                                .setVrfNextHopNextHopAddress
                                                        (Collections.singletonList
                                                                (new VrfNextHopNextHopAddressBuilder()
                                                                        .setNextHopAddress(nextHop)
                                                                        .build()))
                                                .build())
                                        .build())
                                .setPrefix(prefix)
                                .setPrefixLength(prefixLength)
                                .setKey(new VrfPrefixKey(prefix, prefixLength))
                                .build();

                    }
                })).build();

// //        Build the parent data structure for VRF
        final Vrf newRoutes = new VrfBuilder()
                .setVrfName(name)
                .setKey(new VrfKey(name))
                .setAddressFamily(new AddressFamilyBuilder()
                        .setVrfipv4(new Vrfipv4Builder()
                                .setVrfUnicast(new VrfUnicastBuilder()
                                        .setVrfPrefixes(transformedRoutes)
                                        .build())
                                .build())
                        .build())
                .build();

        // invoke edit-config, merge the route list for vrf identified by input.getVrfId()
        writeTransaction.delete(LogicalDatastoreType.CONFIGURATION, routesInstanceId);
//        writeTransaction.merge(LogicalDatastoreType.CONFIGURATION, routesInstanceId, newRoutes);

        // commit
        final CheckedFuture<Void, TransactionCommitFailedException> submit = writeTransaction.submit();
        return Futures.transform(submit, new Function<Void, RpcResult<Void>>() {
            @Override
            public RpcResult<Void> apply(final Void result) {
                LOG.info("{} Route(s) written to {}", input.getRouteList().size(), input.getMountName());
                return SUCCESS;
            }
        });
    }


    @Override
    public Future<RpcResult<Void>> deleteRoutesVrf(DeleteRoutesVrfInput input) {
//            return null;
        final Optional<MountPoint> mountPoint;
        try {
            // Get mount point for specified device
            mountPoint = mountPointService.getMountPoint(mountIds.get(input.getMountName()));
        } catch (ExecutionException e) {
            throw new IllegalArgumentException(e);
        }

        // Get DataBroker API and create a write tx
        final DataBroker dataBroker = mountPoint.get().getService(DataBroker.class).get();
        final WriteTransaction writeTransaction = dataBroker.newWriteOnlyTransaction();

        // Build InstanceIdentifier to point to a specific VRF in the device
        final CiscoIosXrString name = new CiscoIosXrString(input.getVrfId());
        final InstanceIdentifier<Vrf> routesInstanceId;
        routesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                .child(Vrfs.class).child(Vrf.class, new VrfKey(name));

        // Prepare the actual routes for VRF. Transform Rpc input routes into VRF routes
//        final VrfPrefixes transformedRoutes = new VrfPrefixesBuilder()
//                .setVrfPrefix(Lists.transform(input.getRouteList(), new Function<RouteList, VrfPrefix>() {
//
//                    @Override
//                    public VrfPrefix apply(final RouteList input) {
//                        final IpAddressNoZone prefix = new IpAddressNoZone(input.getIpv4Prefix());
//                        final IpAddressNoZone nextHop = new IpAddressNoZone(input.getIpv4NextHop());
//                        final long prefixLength = input.getIpv4PrefixLength();
//                        return new VrfPrefixBuilder()
//                                .setVrfRoute(new VrfRouteBuilder()
//                                        .setVrfNextHopTable(new VrfNextHopTableBuilder()
//                                                .setVrfNextHopNextHopAddress
//                                                        (Collections.singletonList
//                                                                (new VrfNextHopNextHopAddressBuilder()
//                                                                        .setNextHopAddress(nextHop)
//                                                                        .build()))
//                                                .build())
//                                        .build())
//                                .setPrefix(prefix)
//                                .setPrefixLength(prefixLength)
//                                .setKey(new VrfPrefixKey(prefix, prefixLength))
//                                .build();
//
//                    }
//                })).build();


        final VrfPrefixes transformedRoutes = new VrfPrefixesBuilder().build();
        // Build the parent data structure for VRF
        final Vrf newRoutes = new VrfBuilder()
                .setVrfName(name)
                .setKey(new VrfKey(name))
                .setAddressFamily(new AddressFamilyBuilder()
                        .setVrfipv4(new Vrfipv4Builder()
                                .setVrfUnicast(new VrfUnicastBuilder()
                                        .setVrfPrefixes(transformedRoutes)
                                        .build())
                                .build())
                        .build())
                .build();

        // invoke edit-config, merge the route list for vrf identified by input.getVrfId()
//        writeTransaction.delete(LogicalDatastoreType.CONFIGURATION, routesInstanceId);
        writeTransaction.merge(LogicalDatastoreType.CONFIGURATION, routesInstanceId, newRoutes);


        return null;
    }

    @Override
    public Future<RpcResult<Void>> leftrightPolicyIntf(LeftrightPolicyIntfInput input) {

        final Optional<MountPoint> mountPoint;
        try {
            // Get mount point for specified device
            mountPoint = mountPointService.getMountPoint(mountIds.get(input.getMountName()));
        } catch (ExecutionException e) {
            throw new IllegalArgumentException(e);
        }

        // Get DataBroker API and create a write tx
        final DataBroker dataBroker = mountPoint.get().getService(DataBroker.class).get();
//        final DataBroker dataBroker2 = mountPoint.get().getService(DataBroker.class).get();
        final WriteTransaction writeTransactionadd = dataBroker.newWriteOnlyTransaction();
        final WriteTransaction writeTransactiondel = dataBroker.newWriteOnlyTransaction();
        final WriteTransaction writeTransactionadd2 = dataBroker.newWriteOnlyTransaction();
        final WriteTransaction writeTransactiondel2 = dataBroker.newWriteOnlyTransaction();
        int req_number = input.getLeftright().getIntValue();
        String req_name = input.getLeftright().getName();
        LOG.info(" req_number req_number", req_number);
        LOG.info(" req_number req_name", req_name.toString());

        System.out.println(" req_number req_name");
        System.out.println(req_name.toString());
        System.out.println();

        ///////////////////////
//        final IpAddressNoZone prefix = new IpAddressNoZone(input.getIpv4Prefix());
//        final InterfaceName nextHop = new InterfaceName(input.getNextHopInterface());
//        final Long prefix_length = Long.valueOf(input.getIpv4PrefixLength());
//        final Long metric = Long.valueOf(input.getNextHopMetric());

        IpAddressNoZone addprefix;
        InterfaceName addnextHopIntf;
        Long addprefix_length;
        Long addmetric;

        IpAddressNoZone delprefix;
        InterfaceName delnextHopIntf;
        Long delprefix_length;
        Long delmetric;

        IpAddressNoZone addprefix2;
        InterfaceName addnextHopIntf2;
        Long addprefix_length2;
        Long addmetric2;

        IpAddressNoZone delprefix2;
        InterfaceName delnextHopIntf2;
        Long delprefix_length2;
        Long delmetric2;


        if (req_number ==0)
        {

            addprefix = new IpAddressNoZone(input.getIpv4Prefix());
            addnextHopIntf = new InterfaceName(input.getNextHopIntf());
            addprefix_length = Long.valueOf(32);
            addmetric = Long.valueOf(input.getNextHopMetric());

            delprefix = new  IpAddressNoZone(input.getIpv4Prefix2());
            delnextHopIntf = new InterfaceName(input.getNextHopIntf2());
            delprefix_length = Long.valueOf(32);
            delmetric = Long.valueOf(input.getNextHopMetric2());

            final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> deroutesInstanceId;
            deroutesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                    .child(DefaultVrf.class)
                    .child(AddressFamily.class)
                    .child(Vrfipv4.class)
                    .child(VrfUnicast.class)
                    .child(VrfPrefixes.class)
                    .child(VrfPrefix.class, new VrfPrefixKey(delprefix, delprefix_length));

            writeTransactiondel.delete(LogicalDatastoreType.CONFIGURATION, deroutesInstanceId);
//             commit
            final CheckedFuture<Void, TransactionCommitFailedException> submitdel = writeTransactiondel.submit();
            Futures.transform(submitdel, new Function<Void, RpcResult<Void>>() {
                @Override
                public RpcResult<Void> apply(final Void result) {
                    LOG.info("{} WRITE DEFAULT VRF Route(s) written to {}", input.getMountName());
                    return SUCCESS;
                }
            });

            final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> routesInstanceId;
            routesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                    .child(DefaultVrf.class)
                    .child(AddressFamily.class)
                    .child(Vrfipv4.class)
                    .child(VrfUnicast.class)
                    .child(VrfPrefixes.class)
                    .child(VrfPrefix.class, new VrfPrefixKey(addprefix, addprefix_length));


            VrfPrefix vp =  new VrfPrefixBuilder()
                    .setVrfRoute(new VrfRouteBuilder()
                            .setVrfNextHopTable(new VrfNextHopTableBuilder()
                                    .setVrfNextHopInterfaceName
                                            (Collections.singletonList
                                                    (new VrfNextHopInterfaceNameBuilder()
                                                            .setInterfaceName(addnextHopIntf)
                                                            .setLoadMetric((long)addmetric)
                                                            .build()))

                                    .build())
                            .build())
                    .setPrefix(addprefix)
                    .setPrefixLength(addprefix_length)
                    .setKey(new VrfPrefixKey(addprefix, addprefix_length))
                    .build();
            // invoke edit-config, merge the route list for vrf identified by input.getVrfId()
            writeTransactionadd.merge(LogicalDatastoreType.CONFIGURATION, routesInstanceId, vp);
            // commit
            final CheckedFuture<Void, TransactionCommitFailedException> submitadd = writeTransactionadd.submit();
            return Futures.transform(submitadd, new Function<Void, RpcResult<Void>>() {
                @Override
                public RpcResult<Void> apply(final Void result) {
                    LOG.info("{} WRITE DEFAULT VRF Route(s) written to {}", input.getMountName());
                    return SUCCESS;
                }
            });


        }
        if (req_number ==1)
        {

            addprefix = new IpAddressNoZone(input.getIpv4Prefix2());
            addnextHopIntf = new InterfaceName(input.getNextHopIntf2());
            addprefix_length = Long.valueOf(32);
            addmetric = Long.valueOf(input.getNextHopMetric2());

            delprefix = new  IpAddressNoZone(input.getIpv4Prefix());
            delnextHopIntf = new InterfaceName(input.getNextHopIntf());
            delprefix_length = Long.valueOf(32);
            delmetric = Long.valueOf(input.getNextHopMetric());

            final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> deroutesInstanceId;
            deroutesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                    .child(DefaultVrf.class)
                    .child(AddressFamily.class)
                    .child(Vrfipv4.class)
                    .child(VrfUnicast.class)
                    .child(VrfPrefixes.class)
                    .child(VrfPrefix.class, new VrfPrefixKey(delprefix, delprefix_length));

            writeTransactiondel.delete(LogicalDatastoreType.CONFIGURATION, deroutesInstanceId);
//             commit
            final CheckedFuture<Void, TransactionCommitFailedException> submitdel = writeTransactiondel.submit();




            final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> routesInstanceId;
            routesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                    .child(DefaultVrf.class)
                    .child(AddressFamily.class)
                    .child(Vrfipv4.class)
                    .child(VrfUnicast.class)
                    .child(VrfPrefixes.class)
                    .child(VrfPrefix.class, new VrfPrefixKey(addprefix, addprefix_length));


            VrfPrefix vp =  new VrfPrefixBuilder()
                    .setVrfRoute(new VrfRouteBuilder()
                            .setVrfNextHopTable(new VrfNextHopTableBuilder()
                                    .setVrfNextHopInterfaceName
                                            (Collections.singletonList
                                                    (new VrfNextHopInterfaceNameBuilder()
                                                            .setInterfaceName(addnextHopIntf)
                                                            .setLoadMetric((long)addmetric)
                                                            .build()))

                                    .build())
                            .build())
                    .setPrefix(addprefix)
                    .setPrefixLength(addprefix_length)
                    .setKey(new VrfPrefixKey(addprefix, addprefix_length))
                    .build();
            // invoke edit-config, merge the route list for vrf identified by input.getVrfId()
            writeTransactionadd.merge(LogicalDatastoreType.CONFIGURATION, routesInstanceId, vp);

            // commit
            final CheckedFuture<Void, TransactionCommitFailedException> submitadd = writeTransactionadd.submit();
            return Futures.transform(submitadd, new Function<Void, RpcResult<Void>>() {
                @Override
                public RpcResult<Void> apply(final Void result) {
                    LOG.info("{} WRITE DEFAULT VRF Route(s) written to {}", input.getMountName());
                    return SUCCESS;
                }
            });


        }
        else if(req_number ==2)
        {
            addprefix = new IpAddressNoZone(input.getIpv4Prefix());
            addnextHopIntf = new InterfaceName(input.getNextHopIntf());
            addprefix_length = Long.valueOf(32);
            addmetric = Long.valueOf(input.getNextHopMetric());

            addprefix2 = new IpAddressNoZone(input.getIpv4Prefix2());
            addnextHopIntf2 = new InterfaceName(input.getNextHopIntf2());
            addprefix_length2 = Long.valueOf(32);
            addmetric2 = Long.valueOf(input.getNextHopMetric2());

            final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> routesInstanceId;
            routesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                    .child(DefaultVrf.class)
                    .child(AddressFamily.class)
                    .child(Vrfipv4.class)
                    .child(VrfUnicast.class)
                    .child(VrfPrefixes.class)
                    .child(VrfPrefix.class, new VrfPrefixKey(addprefix, addprefix_length));


            VrfPrefix vp =  new VrfPrefixBuilder()
                    .setVrfRoute(new VrfRouteBuilder()
                            .setVrfNextHopTable(new VrfNextHopTableBuilder()
                                    .setVrfNextHopInterfaceName
                                            (Collections.singletonList
                                                    (new VrfNextHopInterfaceNameBuilder()
                                                            .setInterfaceName(addnextHopIntf)
                                                            .setLoadMetric((long)addmetric)
                                                            .build()))

                                    .build())
                            .build())
                    .setPrefix(addprefix)
                    .setPrefixLength(addprefix_length)
                    .setKey(new VrfPrefixKey(addprefix, addprefix_length))
                    .build();
            // invoke edit-config, merge the route list for vrf identified by input.getVrfId()
            writeTransactionadd.merge(LogicalDatastoreType.CONFIGURATION, routesInstanceId, vp);
            // commit
            final CheckedFuture<Void, TransactionCommitFailedException> submitadd = writeTransactionadd.submit();
            System.out.println(Futures.transform(submitadd, new Function<Void, RpcResult<Void>>() {
                @Override
                public RpcResult<Void> apply(final Void result) {
                    LOG.info("{} WRITE DEFAULT VRF Route(s) written to {}", input.getMountName());
                    return SUCCESS;
                }
            }));




            final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> routesInstanceId2;
            routesInstanceId2 = InstanceIdentifier.create(RouterStatic.class)
                    .child(DefaultVrf.class)
                    .child(AddressFamily.class)
                    .child(Vrfipv4.class)
                    .child(VrfUnicast.class)
                    .child(VrfPrefixes.class)
                    .child(VrfPrefix.class, new VrfPrefixKey(addprefix2, addprefix_length2));
            VrfPrefix vp2 =  new VrfPrefixBuilder()
                    .setVrfRoute(new VrfRouteBuilder()
                            .setVrfNextHopTable(new VrfNextHopTableBuilder()
                                    .setVrfNextHopInterfaceName
                                            (Collections.singletonList
                                                    (new VrfNextHopInterfaceNameBuilder()
                                                            .setInterfaceName(addnextHopIntf2)
                                                            .setLoadMetric((long)addmetric2)
                                                            .build()))

                                    .build())
                            .build())
                    .setPrefix(addprefix2)
                    .setPrefixLength(addprefix_length2)
                    .setKey(new VrfPrefixKey(addprefix2, addprefix_length2))
                    .build();
            // invoke edit-config, merge the route list for vrf identified by input.getVrfId()
            writeTransactionadd2.merge(LogicalDatastoreType.CONFIGURATION, routesInstanceId2, vp2);
            // commit
            final CheckedFuture<Void, TransactionCommitFailedException> submitadd2 = writeTransactionadd2.submit();
            return Futures.immediateFuture(SUCCESS);


        }
        else if(req_number ==3)
        {
            delprefix = new  IpAddressNoZone(input.getIpv4Prefix());
            delprefix_length = Long.valueOf(32);

            delprefix2 = new  IpAddressNoZone(input.getIpv4Prefix2());
            delprefix_length2 = Long.valueOf(32);

            final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> deroutesInstanceId;
            deroutesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                    .child(DefaultVrf.class)
                    .child(AddressFamily.class)
                    .child(Vrfipv4.class)
                    .child(VrfUnicast.class)
                    .child(VrfPrefixes.class)
                    .child(VrfPrefix.class, new VrfPrefixKey(delprefix, delprefix_length));

            writeTransactiondel.delete(LogicalDatastoreType.CONFIGURATION, deroutesInstanceId);
//             commit
            final CheckedFuture<Void, TransactionCommitFailedException> submitdel = writeTransactiondel.submit();
            System.out.println(Futures.transform(submitdel, new Function<Void, RpcResult<Void>>() {
                @Override
                public RpcResult<Void> apply(final Void result) {
                    LOG.info("{} WRITE DEFAULT VRF Route(s) written to {}", input.getMountName());
                    return SUCCESS;
                }
            }));


            final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> deroutesInstanceId2;
            deroutesInstanceId2 = InstanceIdentifier.create(RouterStatic.class)
                    .child(DefaultVrf.class)
                    .child(AddressFamily.class)
                    .child(Vrfipv4.class)
                    .child(VrfUnicast.class)
                    .child(VrfPrefixes.class)
                    .child(VrfPrefix.class, new VrfPrefixKey(delprefix2, delprefix_length2));

            writeTransactiondel2.delete(LogicalDatastoreType.CONFIGURATION, deroutesInstanceId2);
//             commit
            final CheckedFuture<Void, TransactionCommitFailedException> submitdel2 = writeTransactiondel2.submit();

            return Futures.immediateFuture(SUCCESS);


        }
        else
        {
            LOG.error("{} that number/action does not exist");
            return null;
        }

    }

    @Override
    public Future<RpcResult<Void>> writePolicy(WritePolicyInput input) {
        final Optional<MountPoint> mountPoint;
        try {
            // Get mount point for specified device
            mountPoint = mountPointService.getMountPoint(mountIds.get(input.getMountName()));
        } catch (ExecutionException e) {
            throw new IllegalArgumentException(e);
        }

        // Get DataBroker API and create a write tx
        final DataBroker dataBroker = mountPoint.get().getService(DataBroker.class).get();
        final WriteTransaction writeTransaction = dataBroker.newWriteOnlyTransaction();

        // Build InstanceIdentifier to point to a specific VRF in the device
        final CiscoIosXrString name = new CiscoIosXrString(input.getPolicyName());
        final InstanceIdentifier<RoutePolicy> policyInstanceId;
        policyInstanceId = InstanceIdentifier.create(RoutingPolicy.class)
                .child(RoutePolicies.class).child(RoutePolicy.class, new RoutePolicyKey(name));
        // Build the parent data structure for VRF



        final RoutePolicy newRoutePolicy = new RoutePolicyBuilder()
                .setRoutePolicyName(name)
                .setKey(new RoutePolicyKey(name))
                .setRplRoutePolicy(new RplPolicy(input.getPassdeny())).build();
//                .setRplRoutePolicy(new RplPolicy(input.getPassdeny())).build();
//                .setRplRoutePolicy(new RplSet("pass")).build();



        // invoke edit-config, merge the route list for vrf identified by input.getVrfId()
        writeTransaction.merge(LogicalDatastoreType.CONFIGURATION, policyInstanceId, newRoutePolicy);

        // commit
        final CheckedFuture<Void, TransactionCommitFailedException> submit = writeTransaction.submit();
        return Futures.transform(submit, new Function<Void, RpcResult<Void>>() {
            @Override
            public RpcResult<Void> apply(final Void result) {
                LOG.info("{} Policy written written to {}", input.getPassdeny(), input.getMountName());
                return SUCCESS;
            }
        });


    }

    @Override
    public Future<RpcResult<Void>> writeRoutesDef(WriteRoutesDefInput input) {
//
        final Optional<MountPoint> mountPoint;
        try {
            // Get mount point for specified device
            mountPoint = mountPointService.getMountPoint(mountIds.get(input.getMountName()));
        } catch (ExecutionException e) {
            throw new IllegalArgumentException(e);
        }

        // Get DataBroker API and create a write tx
        final DataBroker dataBroker = mountPoint.get().getService(DataBroker.class).get();
        final WriteTransaction writeTransaction = dataBroker.newWriteOnlyTransaction();


        final IpAddressNoZone prefix = new IpAddressNoZone(input.getIpv4Prefix());
        final IpAddressNoZone nextHop = new IpAddressNoZone(input.getIpv4NextHop());
        final Long prefix_length = Long.valueOf(input.getIpv4PrefixLength());
////
        final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> routesInstanceId;
        routesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                .child(DefaultVrf.class)
                .child(AddressFamily.class)
                .child(Vrfipv4.class)
                .child(VrfUnicast.class)
                .child(VrfPrefixes.class)
                .child(VrfPrefix.class, new VrfPrefixKey(prefix, prefix_length));


        VrfPrefix vp =  new VrfPrefixBuilder()
                        .setVrfRoute(new VrfRouteBuilder()
                                .setVrfNextHopTable(new VrfNextHopTableBuilder()
                                        .setVrfNextHopNextHopAddress
                                                (Collections.singletonList
                                                        (new VrfNextHopNextHopAddressBuilder()
                                                                .setNextHopAddress(nextHop)
                                                                .build()))
                                        .build())
                                .build())
                        .setPrefix(prefix)
                        .setPrefixLength(prefix_length)
                        .setKey(new VrfPrefixKey(prefix, prefix_length))
                        .build();


//        })).build();
        // invoke edit-config, merge the route list for vrf identified by input.getVrfId()
        writeTransaction.merge(LogicalDatastoreType.CONFIGURATION, routesInstanceId, vp);

        // commit
        final CheckedFuture<Void, TransactionCommitFailedException> submit = writeTransaction.submit();
        return Futures.transform(submit, new Function<Void, RpcResult<Void>>() {
            @Override
            public RpcResult<Void> apply(final Void result) {
                LOG.info("{} DEFAULT VRF Route(s) written to {}", input.getIpv4Prefix(), input.getMountName());
                return SUCCESS;
            }
        });
    }



    @Override
    public Future<RpcResult<Void>> deleteRoutesDef(DeleteRoutesDefInput input) {
//
        final Optional<MountPoint> mountPoint;
        try {
            // Get mount point for specified device
            mountPoint = mountPointService.getMountPoint(mountIds.get(input.getMountName()));
        } catch (ExecutionException e) {
            throw new IllegalArgumentException(e);
        }

        // Get DataBroker API and create a write tx
        final DataBroker dataBroker = mountPoint.get().getService(DataBroker.class).get();
        final WriteTransaction writeTransaction = dataBroker.newWriteOnlyTransaction();


        final IpAddressNoZone prefix = new IpAddressNoZone(input.getIpv4Prefix());
        final IpAddressNoZone nextHop = new IpAddressNoZone(input.getIpv4NextHop());
        final Long prefix_length = Long.valueOf(input.getIpv4PrefixLength());
////
        final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> routesInstanceId;
        routesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                .child(DefaultVrf.class)
                .child(AddressFamily.class)
                .child(Vrfipv4.class)
                .child(VrfUnicast.class)
                .child(VrfPrefixes.class)
                .child(VrfPrefix.class, new VrfPrefixKey(prefix, prefix_length));


//        VrfPrefix vp =  new VrfPrefixBuilder()
//                        .setVrfRoute(new VrfRouteBuilder()
//                                .setVrfNextHopTable(new VrfNextHopTableBuilder()
//                                        .setVrfNextHopNextHopAddress
//                                                (Collections.singletonList
//                                                        (new VrfNextHopNextHopAddressBuilder()
//                                                                .setNextHopAddress(nextHop)
//                                                                .build()))
//                                        .build())
//                                .build())
//                        .setPrefix(prefix)
//                        .setPrefixLength(prefix_length)
//                        .setKey(new VrfPrefixKey(prefix, prefix_length))
//                        .build();


//        })).build();
        // invoke edit-config, merge the route list for vrf identified by input.getVrfId()
        writeTransaction.delete(LogicalDatastoreType.CONFIGURATION, routesInstanceId);

        // commit
        final CheckedFuture<Void, TransactionCommitFailedException> submit = writeTransaction.submit();
        return Futures.transform(submit, new Function<Void, RpcResult<Void>>() {
            @Override
            public RpcResult<Void> apply(final Void result) {
                LOG.info("{} DEFAULT VRF Route(s) written to {}", input.getIpv4Prefix(), input.getMountName());
                return SUCCESS;
            }
        });
    }



    @Override
    public Future<RpcResult<Void>> deletePolicy(DeletePolicyInput input) {
        final Optional<MountPoint> mountPoint;
        try {
            // Get mount point for specified device
            mountPoint = mountPointService.getMountPoint(mountIds.get(input.getMountName()));
        } catch (ExecutionException e) {
            throw new IllegalArgumentException(e);
        }

        // Get DataBroker API and create a write tx
        final DataBroker dataBroker = mountPoint.get().getService(DataBroker.class).get();
        final WriteTransaction writeTransaction = dataBroker.newWriteOnlyTransaction();

        // Build InstanceIdentifier to point to a specific VRF in the device
        final CiscoIosXrString name = new CiscoIosXrString(input.getPolicyName());
        final InstanceIdentifier<RoutePolicy> policyInstanceId;
        policyInstanceId = InstanceIdentifier.create(RoutingPolicy.class)
                .child(RoutePolicies.class).child(RoutePolicy.class, new RoutePolicyKey(name));
        // Build the parent data structure for VRF



//        final RoutePolicy newRoutePolicy = new RoutePolicyBuilder()
//                .setRoutePolicyName(name)
//                .setKey(new RoutePolicyKey(name))
//                .setRplRoutePolicy(new RplPolicy(input.getPassdeny())).build();
//                .setRplRoutePolicy(new RplSet("pass")).build();



        // invoke edit-config, merge the route list for vrf identified by input.getVrfId()
        writeTransaction.delete(LogicalDatastoreType.CONFIGURATION, policyInstanceId);

        // commit
        final CheckedFuture<Void, TransactionCommitFailedException> submit = writeTransaction.submit();
        return Futures.transform(submit, new Function<Void, RpcResult<Void>>() {
            @Override
            public RpcResult<Void> apply(final Void result) {
                LOG.info("{} Policy written written to {}", input.getPassdeny(), input.getMountName());
                return SUCCESS;
            }
        });
    }

    @Override
    public Future<RpcResult<InterfaceStatallOutput>> interfaceStatall(InterfaceStatallInput input) {
        LOG.info("showNode called, input {}", input);

        final Optional<MountPoint> xrNodeOptional = mountPointService.getMountPoint(NETCONF_TOPO_IID
                .child(Node.class, new NodeKey(new NodeId(input.getNodeName()))));
        LOG.info("2showNode called, input {}", input);

        Preconditions.checkArgument(xrNodeOptional.isPresent(),
                "Unable to locate mountpoint: %s, not mounted yet or not configured",
                input.getNodeName());
        final MountPoint xrNode = xrNodeOptional.get();
        LOG.info("3showNode called, input {}", input);

        // Get the DataBroker for the mounted node
        final DataBroker xrNodeBroker = xrNode.getService(DataBroker.class).get();
        // Start a new read only transaction that we will use to read data
        // from the device
        final ReadOnlyTransaction xrNodeReadTx = xrNodeBroker.newReadOnlyTransaction();
        LOG.info("4showNode called, input {}", input);


        LOG.info("7showNode called, input {}", input);



        InstanceIdentifier<Statistics> eidn = InstanceIdentifier.create(EthernetInterface.class).child(Statistics.class);

//                .child(Statistics.class);
//        InstanceIdentifier<Statistics> ethiid =
//                InstanceIdentifier.create(Statistics.class);
        LOG.info("5showNode called, input {}", input);

        Optional<Statistics> ethIf;
        try {
            // Read from a transaction is asynchronous, but a simple
            // get/checkedGet makes the call synchronous
            ethIf = xrNodeReadTx.read(LogicalDatastoreType.OPERATIONAL, eidn).checkedGet();
        } catch (ReadFailedException e) {
            throw new IllegalStateException("Unexpected error reading data from " + input.getNodeName(), e);
        }
        LOG.info("6showNode called,ethIf.toString(): ", ethIf.toString());


        String stats_in_string = "";
        if (ethIf.isPresent()) {

            stats_in_string = ethIf.get().getStatistic().toString();
        }
        LOG.info("6showNode called, stats_in_string: ", stats_in_string);

        // Finally, we build the RPC response with the retrieved data and return
        InterfaceStatallOutput output = new InterfaceStatallOutputBuilder().setUtil(stats_in_string)
                .build();
        return RpcResultBuilder.success(output).buildFuture();

    }

    @Override
        public Future<RpcResult<ListNodesOutput>> listNodes() {
            // This method was used in the original ncmount project.
            // **Use this method for one-off operations, when you need to find out
            // something about the nodes currently in the Netconf Topology. An
            // application that needs to handle netconf node discovery/disappearance,
            // a data change listener over Netconf topology should be used.
            List<Node> nodes;
            ListNodesOutputBuilder outBld = new ListNodesOutputBuilder();

            ReadTransaction tx = dataBroker.newReadOnlyTransaction();

            // EXAMPLE: Get all the nodes from the Netconf Topology operational
            // space.
            try {
                nodes = tx.read(LogicalDatastoreType.OPERATIONAL, NETCONF_TOPO_IID)
                        .checkedGet()
                        .get()
                        .getNode();
            } catch (ReadFailedException e) {
                LOG.error("Failed to read node config from datastore", e);
                throw new IllegalStateException(e);
            }

            List<String> results = new ArrayList<String>();
            for (Node node : nodes) {
                LOG.info("Node: {}", node);
                NetconfNode nnode = node.getAugmentation(NetconfNode.class);
                if (nnode != null) {
                    // We have a Netconf device
                    NetconfNodeConnectionStatus.ConnectionStatus csts = nnode.getConnectionStatus();
                    LOG.info("ConnectionStatus.Connected bijan:????", csts);
                    if (csts == NetconfNodeConnectionStatus.ConnectionStatus.Connected) {
                        List<String> capabilities =
                                nnode.getAvailableCapabilities().getAvailableCapability().stream().map(cp -> {
//                                    return cp.toString();
                                return cp.getCapability();
                                }).collect(Collectors.toList());
                        LOG.info("Capabilities: {}", capabilities);
                    }
                }
                results.add(node.getNodeId().getValue());
            }
            outBld.setNcOperNodes(results);

            return RpcResultBuilder.success(outBld.build()).buildFuture();
        }

    @Override
    public Future<RpcResult<Void>> writeRoutesDefIntf(WriteRoutesDefIntfInput input) {

        final Optional<MountPoint> mountPoint;
        try {
            // Get mount point for specified device
            mountPoint = mountPointService.getMountPoint(mountIds.get(input.getMountName()));
        } catch (ExecutionException e) {
            throw new IllegalArgumentException(e);
        }

        // Get DataBroker API and create a write tx
        final DataBroker dataBroker = mountPoint.get().getService(DataBroker.class).get();
        final WriteTransaction writeTransaction = dataBroker.newWriteOnlyTransaction();


        final IpAddressNoZone prefix = new IpAddressNoZone(input.getIpv4Prefix());
        final InterfaceName nextHop = new InterfaceName(input.getNextHopInterface());
        final Long prefix_length = Long.valueOf(input.getIpv4PrefixLength());
        final Long metric = Long.valueOf(input.getNextHopMetric());
////
        final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> routesInstanceId;
        routesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                .child(DefaultVrf.class)
                .child(AddressFamily.class)
                .child(Vrfipv4.class)
                .child(VrfUnicast.class)
                .child(VrfPrefixes.class)
                .child(VrfPrefix.class, new VrfPrefixKey(prefix, prefix_length));


        VrfPrefix vp =  new VrfPrefixBuilder()
                .setVrfRoute(new VrfRouteBuilder()
                        .setVrfNextHopTable(new VrfNextHopTableBuilder()
                                .setVrfNextHopInterfaceName
                                        (Collections.singletonList
                                                (new VrfNextHopInterfaceNameBuilder()
                                                        .setInterfaceName(nextHop)
                                                        .setLoadMetric((long)metric)
                                                        .build()))

                                .build())
                        .build())
                .setPrefix(prefix)
                .setPrefixLength(prefix_length)
                .setKey(new VrfPrefixKey(prefix, prefix_length))
                .build();


//        })).build();
        // invoke edit-config, merge the route list for vrf identified by input.getVrfId()
        writeTransaction.merge(LogicalDatastoreType.CONFIGURATION, routesInstanceId, vp);

        // commit
        final CheckedFuture<Void, TransactionCommitFailedException> submit = writeTransaction.submit();
        return Futures.transform(submit, new Function<Void, RpcResult<Void>>() {
            @Override
            public RpcResult<Void> apply(final Void result) {
                LOG.info("{} DEFAULT VRF Route(s) written to {}", input.getIpv4Prefix(), input.getMountName());
                return SUCCESS;
            }
        });
    }

    @Override
    public Future<RpcResult<Void>> deleteRoutesDefIntf(DeleteRoutesDefIntfInput input) {
        return null;
    }

    @Override
    public Future<RpcResult<InterfaceUtilOutput>> interfaceUtil(InterfaceUtilInput input) {
        LOG.info("showNode called, input {}", input);

        final Optional<MountPoint> xrNodeOptional = mountPointService.getMountPoint(NETCONF_TOPO_IID
                .child(Node.class, new NodeKey(new NodeId(input.getNodeName()))));
        LOG.info("2showNode called, input {}", input);

        Preconditions.checkArgument(xrNodeOptional.isPresent(),
                "Unable to locate mountpoint: %s, not mounted yet or not configured",
                input.getNodeName());
        final MountPoint xrNode = xrNodeOptional.get();
        LOG.info("3showNode called, input {}", input);

        // Get the DataBroker for the mounted node
        final DataBroker xrNodeBroker = xrNode.getService(DataBroker.class).get();
        // Start a new read only transaction that we will use to read data
        // from the device
        final ReadOnlyTransaction xrNodeReadTx = xrNodeBroker.newReadOnlyTransaction();
        LOG.info("4showNode called, input {}", input);

        InstanceIdentifier<Statistics> eidn = InstanceIdentifier.create(EthernetInterface.class).child(Statistics.class);

        LOG.info("5showNode called, input {}", input);

        Optional<Statistics> ethIf;
        try {
            // Read from a transaction is asynchronous, but a simple
            // get/checkedGet makes the call synchronous
            ethIf = xrNodeReadTx.read(LogicalDatastoreType.OPERATIONAL, eidn).checkedGet();
        } catch (ReadFailedException e) {
            throw new IllegalStateException("Unexpected error reading data from " + input.getNodeName(), e);
        }
        LOG.info("6showNode called,ethIf.toString(): ", ethIf.toString());

        List<Ifo> ifoList = new ArrayList<Ifo>();

//        List<Statistic> ifstat = new ArrayList<>();
        String stats_in_string = "";
        String interface_name = "";

        if (ethIf.isPresent()) {
            List<Statistic> ifstats = ethIf
                    .get().getStatistic();
            for (Statistic config : ifstats) {
                LOG.info("ConfigReader for '{}': config {}",
                        config.getInterfaceName().getValue(), config);
                String name = config.getInterfaceName().getValue();
                ifoList.add(new IfoBuilder()
                        .setInterfaceName(name.toString())
                        .setReceivedTotalFrames(config.getReceivedTotalFrames().toString())
                        .setTotalFramesTransmitted(config.getTotalFramesTransmitted().toString())
                        .build());
            }
        } else {
            LOG.info("No data present on path '{}' for mountpoint: {}",
                    eidn,
                    input.getNodeName());
        }

        InterfaceUtilOutput output = new InterfaceUtilOutputBuilder()
                        .setIfo(ifoList)
                        .build();
        return RpcResultBuilder.success(output).buildFuture();

    }

    @Override
        public Future<RpcResult<CountNodesOutput>> countNodes() {
            return null;
        }

    public void runner()
    {

    }

    @Override
    public Future<RpcResult<Void>> leftrightPolicy(LeftrightPolicyInput input) {
        final Optional<MountPoint> mountPoint;
        try {
            // Get mount point for specified device
            mountPoint = mountPointService.getMountPoint(mountIds.get(input.getMountName()));
        } catch (ExecutionException e) {
            throw new IllegalArgumentException(e);
        }

        // Get DataBroker API and create a write tx
        final DataBroker dataBroker = mountPoint.get().getService(DataBroker.class).get();
        final WriteTransaction writeTransactionadd = dataBroker.newWriteOnlyTransaction();
        final WriteTransaction writeTransactiondel = dataBroker.newWriteOnlyTransaction();
        final WriteTransaction writeTransactionadd2 = dataBroker.newWriteOnlyTransaction();
        final WriteTransaction writeTransactiondel2 = dataBroker.newWriteOnlyTransaction();
        int req_number = input.getLeftright().getIntValue();
        String req_name = input.getLeftright().getName();
        LOG.info(" req_number req_number", req_number);
        LOG.info(" req_number req_name", req_name.toString());

        System.out.println(" req_number req_name");
        System.out.println(req_name.toString());
        System.out.println();

        IpAddressNoZone addprefix;
        IpAddressNoZone addnextHop;
        Long addprefix_length;
        IpAddressNoZone delprefix;
        IpAddressNoZone delnextHop;
        Long delprefix_length;
        IpAddressNoZone addprefix2;
        IpAddressNoZone addnextHop2;
        Long addprefix_length2;
        IpAddressNoZone delprefix2;
        IpAddressNoZone delnextHop2;
        Long delprefix_length2;


        if (req_number ==0)
        {
            addprefix = new IpAddressNoZone(Ipv4AddressNoZone.getDefaultInstance(node3));
            addnextHop = new IpAddressNoZone(Ipv4AddressNoZone.getDefaultInstance(node2));
            addprefix_length = Long.valueOf(32);
            delprefix = new IpAddressNoZone(Ipv4AddressNoZone.getDefaultInstance(node4));
            delnextHop = new IpAddressNoZone(Ipv4AddressNoZone.getDefaultInstance(node2));
            delprefix_length = Long.valueOf(32);


            final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> deroutesInstanceId;
            deroutesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                    .child(DefaultVrf.class)
                    .child(AddressFamily.class)
                    .child(Vrfipv4.class)
                    .child(VrfUnicast.class)
                    .child(VrfPrefixes.class)
                    .child(VrfPrefix.class, new VrfPrefixKey(delprefix, delprefix_length));

            writeTransactiondel.delete(LogicalDatastoreType.CONFIGURATION, deroutesInstanceId);
//             commit
            final CheckedFuture<Void, TransactionCommitFailedException> submitdel = writeTransactiondel.submit();
//            return Futures.transform(submitdel, new Function<Void, RpcResult<Void>>() {
//                @Override
//                public RpcResult<Void> apply(final Void result) {
//                    LOG.info("{} DELETE DEFAULT VRF Route(s) written to {}", delnextHop, input.getMountName());
//                    return SUCCESS;
//                }
//            });

            final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> routesInstanceId;
            routesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                    .child(DefaultVrf.class)
                    .child(AddressFamily.class)
                    .child(Vrfipv4.class)
                    .child(VrfUnicast.class)
                    .child(VrfPrefixes.class)
                    .child(VrfPrefix.class, new VrfPrefixKey(addprefix, addprefix_length));

            VrfPrefix vp =  new VrfPrefixBuilder()
                    .setVrfRoute(new VrfRouteBuilder()
                            .setVrfNextHopTable(new VrfNextHopTableBuilder()
                                    .setVrfNextHopNextHopAddress
                                            (Collections.singletonList
                                                    (new VrfNextHopNextHopAddressBuilder()
                                                            .setNextHopAddress(addnextHop)
                                                            .build()))
                                    .build())
                            .build())
                    .setPrefix(addprefix)
                    .setPrefixLength(addprefix_length)
                    .setKey(new VrfPrefixKey(addprefix, addprefix_length))
                    .build();
            // invoke edit-config, merge the route list for vrf identified by input.getVrfId()
            writeTransactionadd.merge(LogicalDatastoreType.CONFIGURATION, routesInstanceId, vp);

            // commit
            final CheckedFuture<Void, TransactionCommitFailedException> submitadd = writeTransactionadd.submit();
            return Futures.transform(submitadd, new Function<Void, RpcResult<Void>>() {
                @Override
                public RpcResult<Void> apply(final Void result) {
                    LOG.info("{} WRITE DEFAULT VRF Route(s) written to {}", delnextHop, input.getMountName());
                    return SUCCESS;
                }
            });


        }
        if (req_number ==1)
        {
            addprefix = new IpAddressNoZone(Ipv4AddressNoZone.getDefaultInstance(node4));
            addnextHop = new IpAddressNoZone(Ipv4AddressNoZone.getDefaultInstance(node2));
            addprefix_length = Long.valueOf(32);
            delprefix = new IpAddressNoZone(Ipv4AddressNoZone.getDefaultInstance(node3));
            delnextHop = new IpAddressNoZone(Ipv4AddressNoZone.getDefaultInstance(node2));
            delprefix_length = Long.valueOf(32);


            final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> deroutesInstanceId;
            deroutesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                    .child(DefaultVrf.class)
                    .child(AddressFamily.class)
                    .child(Vrfipv4.class)
                    .child(VrfUnicast.class)
                    .child(VrfPrefixes.class)
                    .child(VrfPrefix.class, new VrfPrefixKey(delprefix, delprefix_length));

            writeTransactiondel.delete(LogicalDatastoreType.CONFIGURATION, deroutesInstanceId);
//             commit
            final CheckedFuture<Void, TransactionCommitFailedException> submitdel = writeTransactiondel.submit();


            final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> routesInstanceId;
            routesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                    .child(DefaultVrf.class)
                    .child(AddressFamily.class)
                    .child(Vrfipv4.class)
                    .child(VrfUnicast.class)
                    .child(VrfPrefixes.class)
                    .child(VrfPrefix.class, new VrfPrefixKey(addprefix, addprefix_length));

            VrfPrefix vp =  new VrfPrefixBuilder()
                    .setVrfRoute(new VrfRouteBuilder()
                            .setVrfNextHopTable(new VrfNextHopTableBuilder()
                                    .setVrfNextHopNextHopAddress
                                            (Collections.singletonList
                                                    (new VrfNextHopNextHopAddressBuilder()
                                                            .setNextHopAddress(addnextHop)
                                                            .build()))
                                    .build())
                            .build())
                    .setPrefix(addprefix)
                    .setPrefixLength(addprefix_length)
                    .setKey(new VrfPrefixKey(addprefix, addprefix_length))
                    .build();
            // invoke edit-config, merge the route list for vrf identified by input.getVrfId()
            writeTransactionadd.merge(LogicalDatastoreType.CONFIGURATION, routesInstanceId, vp);

            // commit
            final CheckedFuture<Void, TransactionCommitFailedException> submitadd = writeTransactionadd.submit();

            return Futures.transform(submitadd, new Function<Void, RpcResult<Void>>() {
                @Override
                public RpcResult<Void> apply(final Void result) {
                    LOG.info("{} WRITE DEFAULT VRF Route(s) written to {}", delnextHop, input.getMountName());
                    return SUCCESS;
                }
            });
        }
        else if(req_number ==2)
        {
            addprefix = new IpAddressNoZone(Ipv4AddressNoZone.getDefaultInstance(node3));
            addnextHop = new IpAddressNoZone(Ipv4AddressNoZone.getDefaultInstance(node2));
            addprefix_length = Long.valueOf(32);
            addprefix2 = new IpAddressNoZone(Ipv4AddressNoZone.getDefaultInstance(node4));
            addnextHop2 = new IpAddressNoZone(Ipv4AddressNoZone.getDefaultInstance(node2));
            addprefix_length2 = Long.valueOf(32);

            final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> routesInstanceId;
            routesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                    .child(DefaultVrf.class)
                    .child(AddressFamily.class)
                    .child(Vrfipv4.class)
                    .child(VrfUnicast.class)
                    .child(VrfPrefixes.class)
                    .child(VrfPrefix.class, new VrfPrefixKey(addprefix, addprefix_length));

            VrfPrefix vp =  new VrfPrefixBuilder()
                    .setVrfRoute(new VrfRouteBuilder()
                            .setVrfNextHopTable(new VrfNextHopTableBuilder()
                                    .setVrfNextHopNextHopAddress
                                            (Collections.singletonList
                                                    (new VrfNextHopNextHopAddressBuilder()
                                                            .setNextHopAddress(addnextHop)
                                                            .build()))
                                    .build())
                            .build())
                    .setPrefix(addprefix)
                    .setPrefixLength(addprefix_length)
                    .setKey(new VrfPrefixKey(addprefix, addprefix_length))
                    .build();
            // invoke edit-config, merge the route list for vrf identified by input.getVrfId()
            writeTransactionadd.merge(LogicalDatastoreType.CONFIGURATION, routesInstanceId, vp);
            // commit
            final CheckedFuture<Void, TransactionCommitFailedException> submitadd = writeTransactionadd.submit();


            final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> routesInstanceId2;
            routesInstanceId2 = InstanceIdentifier.create(RouterStatic.class)
                    .child(DefaultVrf.class)
                    .child(AddressFamily.class)
                    .child(Vrfipv4.class)
                    .child(VrfUnicast.class)
                    .child(VrfPrefixes.class)
                    .child(VrfPrefix.class, new VrfPrefixKey(addprefix2, addprefix_length2));

            VrfPrefix vp2 =  new VrfPrefixBuilder()
                    .setVrfRoute(new VrfRouteBuilder()
                            .setVrfNextHopTable(new VrfNextHopTableBuilder()
                                    .setVrfNextHopNextHopAddress
                                            (Collections.singletonList
                                                    (new VrfNextHopNextHopAddressBuilder()
                                                            .setNextHopAddress(addnextHop2)
                                                            .build()))
                                    .build())
                            .build())
                    .setPrefix(addprefix2)
                    .setPrefixLength(addprefix_length2)
                    .setKey(new VrfPrefixKey(addprefix2, addprefix_length2))
                    .build();
            // invoke edit-config, merge the route list for vrf identified by input.getVrfId()
            writeTransactionadd2.merge(LogicalDatastoreType.CONFIGURATION, routesInstanceId2, vp2);
            // commit
            final CheckedFuture<Void, TransactionCommitFailedException> submitadd2 = writeTransactionadd2.submit();
            return Futures.transform(submitadd, new Function<Void, RpcResult<Void>>() {
                @Override
                public RpcResult<Void> apply(final Void result) {
                    LOG.info("{} WRITE DEFAULT VRF Route(s) written to {}", input.getMountName());
                    return SUCCESS;
                }
            });
        }
        else if(req_number ==3)
        {
            delprefix = new IpAddressNoZone(Ipv4AddressNoZone.getDefaultInstance(node3));
            delprefix_length = Long.valueOf(32);
            delprefix2 = new IpAddressNoZone(Ipv4AddressNoZone.getDefaultInstance(node4));
            delprefix_length2 = Long.valueOf(32);


            final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> deroutesInstanceId;
            deroutesInstanceId = InstanceIdentifier.create(RouterStatic.class)
                    .child(DefaultVrf.class)
                    .child(AddressFamily.class)
                    .child(Vrfipv4.class)
                    .child(VrfUnicast.class)
                    .child(VrfPrefixes.class)
                    .child(VrfPrefix.class, new VrfPrefixKey(delprefix, delprefix_length));

            writeTransactiondel.delete(LogicalDatastoreType.CONFIGURATION, deroutesInstanceId);
//             commit
            final CheckedFuture<Void, TransactionCommitFailedException> submitdel = writeTransactiondel.submit();



            final KeyedInstanceIdentifier<VrfPrefix, VrfPrefixKey> deroutesInstanceId2;
            deroutesInstanceId2 = InstanceIdentifier.create(RouterStatic.class)
                    .child(DefaultVrf.class)
                    .child(AddressFamily.class)
                    .child(Vrfipv4.class)
                    .child(VrfUnicast.class)
                    .child(VrfPrefixes.class)
                    .child(VrfPrefix.class, new VrfPrefixKey(delprefix2, delprefix_length2));

            writeTransactiondel2.delete(LogicalDatastoreType.CONFIGURATION, deroutesInstanceId2);
//             commit
            final CheckedFuture<Void, TransactionCommitFailedException> submitdel2 = writeTransactiondel2.submit();

            return Futures.transform(submitdel2, new Function<Void, RpcResult<Void>>() {
                @Override
                public RpcResult<Void> apply(final Void result) {
                    LOG.info("{} WRITE DEFAULT VRF Route(s) written to {}", input.getMountName());
                    return SUCCESS;
                }
            });
        }
        else
        {
            LOG.error("{} that number/action does not exist");
            return null;
        }

    }


    @Override
    public Future<RpcResult<ShowNodeOutput>> showNode(ShowNodeInput input) {
        LOG.info("showNode called, input {}", input);

        // Get the mount point for the specified node
        // Equivalent to '.../restconf/<config | operational>/opendaylight-inventory:nodes/node/<node-name>/yang-ext:mount/'
        // Note that we can read both config and operational data from the same
        // mount point

        final Optional<MountPoint> xrNodeOptional = mountPointService.getMountPoint(NETCONF_TOPO_IID
                .child(Node.class, new NodeKey(new NodeId(input.getNodeName()))));
        LOG.info("2showNode called, input {}", input);

        Preconditions.checkArgument(xrNodeOptional.isPresent(),
                "Unable to locate mountpoint: %s, not mounted yet or not configured",
                input.getNodeName());
        final MountPoint xrNode = xrNodeOptional.get();
        LOG.info("3showNode called, input {}", input);

        // Get the DataBroker for the mounted node
        final DataBroker xrNodeBroker = xrNode.getService(DataBroker.class).get();
        // Start a new read only transaction that we will use to read data
        // from the device
        final ReadOnlyTransaction xrNodeReadTx = xrNodeBroker.newReadOnlyTransaction();
        LOG.info("4showNode called, input {}", input);

        // EXAMPLE: Browsing through the node's interface configuration data.

        // First, we get an Instance Identifier for the configuration data. Note
        // that the Instance Identifier is relative to the mountpoint (we got
        // the path to the mountpoint above). The Instance Identifier path is
        // equivalent to:
        // '.../yang-ext:mount/Cisco-IOS-XR-ifmgr-cfg:interface-configurations'
        InstanceIdentifier<InterfaceConfigurations> iid =
                InstanceIdentifier.create(InterfaceConfigurations.class);
        LOG.info("5showNode called, input {}", input);

        Optional<InterfaceConfigurations> ifConfig;
        try {
            // Read from a transaction is asynchronous, but a simple
            // get/checkedGet makes the call synchronous
            ifConfig = xrNodeReadTx.read(LogicalDatastoreType.CONFIGURATION, iid).checkedGet();
        } catch (ReadFailedException e) {
            throw new IllegalStateException("Unexpected error reading data from " + input.getNodeName(), e);
        }
        LOG.info("6showNode called, input {}", input);

        List<Ifc> ifcList = new ArrayList<Ifc>();
        if (ifConfig.isPresent()) {
            List<InterfaceConfiguration> ifConfigs = ifConfig
                    .get()
                    .getInterfaceConfiguration();
            for (InterfaceConfiguration config : ifConfigs) {
                LOG.info("ConfigReader for '{}': config {}",
                        config.getInterfaceName().getValue(), config);
                String ifcActive = config.getActive().getValue();
                String ifcName = config.getInterfaceName().getValue();
                ifcList.add(new IfcBuilder()
                        .setActive(ifcActive)
                        .setBandwidth(config.getBandwidth())
                        .setDescription(config.getDescription())
                        .setInterfaceName(ifcName)
                        .setLinkStatus(config.isLinkStatus() == Boolean.TRUE ? "Up" : "Down")
                        .setKey(new IfcKey(ifcActive, ifcName))
                        .build());
            }
        } else {
            LOG.info("No data present on path '{}' for mountpoint: {}",
                    iid,
                    input.getNodeName());
        }
        LOG.info("7showNode called, input {}", input);

        // EXAMPLE: Browsing through the node's interface operational data

        // First, we get an Instance Identifier for the portion of the
        // operational data that we want to browse through. Note that we are
        // getting an identifier to a more specific path - the data-nodes
        // container within the interface-properties container. The Instance
        // Identifier path is equivalent to:
        // '.../yang-ext:mount/Cisco-IOS-XR-ifmgr-oper:interface-properties/data-nodes'
        InstanceIdentifier<DataNodes> idn = InstanceIdentifier.create(InterfaceProperties.class)
                .child(DataNodes.class);
        Optional<DataNodes> ldn;
        try {
            ldn = xrNodeReadTx.read(LogicalDatastoreType.OPERATIONAL, idn).checkedGet();
        } catch (ReadFailedException e) {
            throw new IllegalStateException("Unexpected error reading data from " + input.getNodeName(), e);
        }
        LOG.info("8showNode called, input {}", input);

        if (ldn.isPresent()) {
            List<DataNode> dataNodes = ldn.get().getDataNode();
            for (DataNode node : dataNodes) {
                LOG.info("DataNode '{}'", node.getDataNodeName().getValue());

                Locationviews lw = node.getLocationviews();
                List<Locationview> locationViews = lw.getLocationview();
                for (Locationview view : locationViews) {
                    LOG.info("LocationView '{}': {}",
                            view.getKey().getLocationviewName().getValue(),
                            view);
                }

                Interfaces ifc = node.getSystemView().getInterfaces();
                List<Interface> ifList = ifc.getInterface();
                for (Interface intf : ifList) {
                    LOG.info("Interface '{}': {}",
                            intf.getInterface().getValue(), intf);
                }

            }
        } else {
            LOG.info("No data present on path '{}' for mountpoint: {}",
                    idn, input.getNodeName());
        }
        LOG.info("9showNode called, input {}", input);

        // Finally, we build the RPC response with the retrieved data and return
        ShowNodeOutput output = new ShowNodeOutputBuilder()
                .setIfCfgData(new IfCfgDataBuilder()
                        .setIfc(ifcList)
                        .build())
                .build();
        return RpcResultBuilder.success(output).buildFuture();
    }

    LoadingCache<String, KeyedInstanceIdentifier<Node, NodeKey>> mountIds = CacheBuilder.newBuilder()
            .maximumSize(20)
            .build(
                    new CacheLoader<String, KeyedInstanceIdentifier<Node, NodeKey>>() {
                        public KeyedInstanceIdentifier<Node, NodeKey> load(final String key) {
                            return NETCONF_TOPO_IID.child(Node.class, new NodeKey(new NodeId(key)));
                        }
                    });
    /**
     * Determines the Netconf Node Node ID, given the node's instance
     * identifier.
     *
     * @param path Node's instance identifier
     * @return NodeId for the node
     */



    private NodeId getNodeId(final InstanceIdentifier<?> path) {
        for (InstanceIdentifier.PathArgument pathArgument : path.getPathArguments()) {
            if (pathArgument instanceof InstanceIdentifier.IdentifiableItem<?, ?>) {

                final Identifier key = ((InstanceIdentifier.IdentifiableItem) pathArgument).getKey();
                if (key instanceof NodeKey) {
                    return ((NodeKey) key).getNodeId();
                }
            }
        }
        return null;
    }
    @Override
    public void onDataChanged(AsyncDataChangeEvent<InstanceIdentifier<?>, DataObject> change) {
        //TODO: this method throws erros when getting capabilities.
        // We need to handle the following types of events:
        // 1. Discovery of new nodes
        // 2. Status change in existing nodes
        // 3. Removal of existing nodes
        LOG.info("OnDataChange, change: {}", change);

        // EXAMPLE: New node discovery
        // React to new Netconf nodes added to the Netconf topology or existing
        // Netconf nodes deleted from the Netconf topology
        for (Map.Entry<InstanceIdentifier<?>,
                DataObject> entry : change.getCreatedData().entrySet()) {
            if (entry.getKey().getTargetType() == NetconfNode.class) {
                NodeId nodeId = getNodeId(entry.getKey());
                LOG.info("NETCONF Node: {} was created", nodeId.getValue());

                // Not much can be done at this point, we need UPDATE event with
                // state set to connected
            }
        }

        // EXAMPLE: Status change in existing node(s)
        // React to data changes in Netconf nodes that are present in the
        // Netconf topology
        for (Map.Entry<InstanceIdentifier<?>,
                DataObject> entry : change.getUpdatedData().entrySet()) {
            if (entry.getKey().getTargetType() == NetconfNode.class) {
                NodeId nodeId = getNodeId(entry.getKey());

                // We have a Netconf device
                NetconfNode nnode = (NetconfNode) entry.getValue();
                NetconfNodeConnectionStatus.ConnectionStatus csts = nnode.getConnectionStatus();

                switch (csts) {
                    case Connected: {
                        // Fully connected, all services for remote device are
                        // available from the MountPointService.
                        LOG.info("NETCONF Node: {} is fully connected", nodeId.getValue());
                        List<String> capabilities =
                                nnode.getAvailableCapabilities().getAvailableCapability().stream().map(cp ->
                                        cp.getCapability()).collect(Collectors.toList());
                        LOG.info("Capabilities: {}", capabilities);

                        // Check if device supports our example notification and if it does, register a notification listener
                        if (capabilities.contains(QName.create(VrfRouteNotification.QNAME, "Example-notifications").toString())) {
                            registerNotificationListener(nodeId);
                        }

                        break;
                    }
                    case Connecting: {
                        // A Netconf device's will be in the 'Connecting' state
                        // initially, and go back to it after disconnect.
                        // Note that a device could be moving back and forth
                        // between the 'Connected' and 'Connecting' states for
                        // various reasons, such as disconnect from remote
                        // device, network connectivity loss etc.
                        LOG.info("NOTIFICATION EXAMPLE: NETCONF Node: {} was disconnected", nodeId.getValue());
                        break;
                    }
                    case UnableToConnect: {
                        // The maximum configured number of reconnect attempts
                        // have been reached. No more reconnects will be
                        // attempted by the Netconf Connector.
                        LOG.info("NOTIFICATION EXAMPLE: NETCONF Node: {} connection failed", nodeId.getValue());
                        break;
                    }
                }
            }
        }

        // EXAMPLE: Removal of an existing node from the Netconf topology
        for (InstanceIdentifier<?> removedPath : change.getRemovedPaths()) {
            final NodeId nodeId = getNodeId(removedPath);
            if (nodeId != null) {
                // A User removed the Netconf connector for this node
                // Before a node is removed, it changes its state to connecting
                // (just as if it was disconnected). We may see this multiple
                // times, since our listener is scoped to SUBTREE.
                LOG.info("NOTIFICATION EXAMPLE:  NETCONF Node: {} was removed", nodeId.getValue());
            }
        }
    }

    private void registerNotificationListener(final NodeId nodeId) {
        final Optional<MountPoint> mountPoint;
        try {
            // Get mount point for specified device
            mountPoint = mountPointService.getMountPoint(mountIds.get(nodeId.getValue()));
        } catch (ExecutionException e) {
            throw new IllegalArgumentException(e);
        }

        // Instantiate notification listener
        final ExampleNotificationsListener listener;
        // The PerformanceAwareNotificationListener is a special version of listener that
        // measures the time until a specified number of notifications was received
        // The performance is calculated as number of received notifications / elapsed time in seconds
        // This is used for performance testing/measurements and can be ignored
        if (PerformanceAwareNotificationListener.shouldMeasurePerformance(nodeId) &&
                !nodeId.getValue().equals("controller-config")/*exclude loopback netconf connection*/) {
            listener = new PerformanceAwareNotificationListener(nodeId);
        } else {
            // Regular simple notification listener with a simple log message
            listener = new LoggingNotificationListener();
        }

        // Register notification listener
        final Optional<NotificationService> service1 = mountPoint.get().getService(NotificationService.class);
        LOG.info("Registering notification listener on {} for node: {}", VrfRouteNotification.QNAME, nodeId);
        final ListenerRegistration<ExampleNotificationsListener> accessTopologyListenerListenerRegistration =
                service1.get().registerNotificationListener(listener);

        // We have the listener registered, but we need to start the notification stream from device by
        // invoking the create-subscription rpc with for stream named "STREAM_NAME". "STREAM_NAME" is not a valid
        // stream name and serves only for demonstration
        // ---
        // This snippet also demonstrates how to invoke custom RPCs on a mounted netconf node
        // The rpc being invoked here can be found at: https://tools.ietf.org/html/rfc5277#section-2.1.1
        // Note that there is no yang model for it in ncmount, but it is in org.opendaylight.controller:ietf-netconf-notifications
        // which is a transitive dependency in ncmount-impl
        final String streamName = "STREAM_NAME";
        final Optional<RpcConsumerRegistry> service = mountPoint.get().getService(RpcConsumerRegistry.class);
        final NotificationsService rpcService = service.get().getRpcService(NotificationsService.class);
        final CreateSubscriptionInputBuilder createSubscriptionInputBuilder = new CreateSubscriptionInputBuilder();
        createSubscriptionInputBuilder.setStream(new StreamNameType(streamName));
        LOG.info("Triggering notification stream {} for node {}", streamName, nodeId);
        final Future<RpcResult<Void>> subscription = rpcService.createSubscription(createSubscriptionInputBuilder.build());
    }
}