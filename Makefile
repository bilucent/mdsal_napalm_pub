help:
	@echo
	@echo "Please use \`make <target>' where <target> is one of"
	@echo
	@echo "  build_odl    to use mvn build"
	@echo "  clear_odl    to use mvn clean"
	@echo "  gen_reqs     to generate the pip requirements file in etc/"
	@echo "  autopep      to fix coding style in the project"
	@echo "  clear_pyc    to remove all pyc"


autopep:
	autopep8 --in-place --aggressive --aggressive -r .

clear_pyc:
	find . -name *.pyc* -type f -delete

gen_reqs:
	pipreqs .

build_odl:
	mvn install -DskipTests

clear_odl:
	mvn clean
