import time

from confy_napalm.napalm_driver import NapalmDriver
from simple_client.src.rate_monitor import ODLMonitor
from simple_client.src.steering_policy import ODLSteering
from utils.config import read_config


def run_steering_policy():
    conf = read_config()
    odl_class = ODLSteering(conf)
    while (1):
        rate = odl_class.simple_rate_cal(conf["intf1"])
        action, metric1, metric2 = odl_class.te_logic(rate)
        print(action, metric1, metric2)
        odl_class.right_left_op("policy-name", action)
        time.sleep(5)  # delays for 5 seconds so the policy changing is not very aggressive


def run_rate_monitor():
    conf = read_config()
    odl_class = ODLMonitor(conf)
    while (True):
        rate0 = odl_class.simple_rate_cal(conf["intf0"])
        rate1 = odl_class.simple_rate_cal(conf["intf1"])
        rate2 = odl_class.simple_rate_cal(conf["intf2"])
        print("  intrface rates below: ")
        print("     rate0 ", rate0)
        print("     rate1 ", rate1)
        print("     rate2 ", rate2)
        time.sleep(5)


def run_napalm():
    conf = read_config()
    napalm = NapalmDriver("iosxr", **conf)
    napalm.drop()
    while (True):
        rate = napalm.simple_rate_cal(napalm.dev_mon, conf["intf0"])
        action, metric1, metric2 = napalm.te_logic(rate)
        print(action, metric1, metric2)
        napalm.right_left("policy-name", action, metric1, metric2)
        time.sleep(2)
    # napalm.dev_policy.close()
    # napalm.dev_mon.close()


if __name__ == "__main__":

    # change this wit argparse
    option = input("choose: \n"
                   "1 for napalm, \n"
                   "2 for ODL steering, \n"
                   "3 for rate monitoring \n"
                   ": ")
    print(option)
    if option == "1":
        run_napalm()
    elif option == "2":
        run_steering_policy()
    elif option == "3":
        run_rate_monitor()
    else:
        print("bad inputs, try again")
