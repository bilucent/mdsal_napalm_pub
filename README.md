Introduction
====
This project shows how to use yang models and MDSAL from OpenDaylight SDN controller to build a network service with high-level and low-level API access.
This separation of APIs allows for separation of designer and operators for accessing and modifying services runnong on top of the network.
The southbound drivers used are Netconf and BGP.



## Yang model:
The yang model defines the following RPCs which is a mix of high/low level services:
````
rpcs:
  +---x write-routes
  |     ...
  +---x write-routesDef
  |     ...
  +---x delete-routesDef
  |     ...
  +---x write-routesDef-intf
  |     ...
  +---x delete-routesDef-intf
  |     ...
  +---x delete-routes-vrf
  |     ...
  +---x delete-routes
  |     ...
  +---x leftright-policy
  |     ...
  +---x leftright-policy-intf
  |     ...
  +---x write-policy
  |     ...
  +---x delete-policy
  |     ...
  +---x interface-statall
  |     ...
  +---x interface-util
  |     ...
  +---x show-node
  |     ...
  +---x list-nodes
  |     ...
  +---x count-nodes
        ...

````

For the purpose of this demonstration, high level services such as  'leftright' is provided (below).
The naming is intentioannly simplistic to emphasize on how high level and button-like a service can be defined.

 This service abstracts the low-level services working as a wrapper method. This helps the users who do not need to know the details to still be able to 'operate' the network. Whilst service designer can access the and modify the low level services separately.


````
...
+---x leftright-policy-intf
|  +---w input
|     +---w mount-name?         string
|     +---w policy-name?        string
|     +---w leftright?          policy-mode-enum
|     +---w ipv4-prefix?        inet:ip-address-no-zone
|     +---w ipv4-prefix2?       inet:ip-address-no-zone
|     +---w next-hop-intf?      string
|     +---w next-hop-intf2?     string
|     +---w next-hop-metric?    uint32
|     +---w next-hop-metric2?   uint32
+---x write-policy
|  +---w input
|     +---w mount-name?    string
|     +---w policy-name?   string
|     +---w passdeny?      string
+---x delete-policy
|  +---w input
|     +---w mount-name?    string
|     +---w policy-name?   string
|     +---w passdeny?      string
+---x interface-statall
|  +---w input
|  |  +---w node-name    string
|  +--ro output
|     +--ro util?   string
+---x interface-util
|  +---w input
|  |  +---w node-name         string
|  |  +---w interface-name    string
|  +--ro output
|     +--ro ifo* []
|        +--ro interface-name?             string
|        +--ro total-frames-transmitted?   string
|        +--ro received-total-frames?      string
...
````

* Pyang is used to generate the tree models:

* link: <https://github.com/mbj4668/pyang>

### Topology

The following topology has been used for the demonstration.
Virtual devices were used for this. But any virtualised or physical environemnet can be used, given that the services below are installed:
Python >2.7
Opendaylight >=6.0 (carbon)
SSH and Netconf, BGP, ISIS/OSPF.

````
          +-------------+
          |             |
          | ODL/Python  |
          |             |
       +--+-------------+-----+
       |                      |              +--------------+
       |                      |              |              |
       |                      |     +--------+ quagga       |
       |                      |     |        |     3.3.3.3  |
+------+----+          +------+-----+        +--------------+
|           |          |            |
|xr1        |          |xr2         |
|   1.1.1.1 |          |   2.2.2.2  |
|           |          |            |
+-----------+          +------------+
                                    |        +--------------+
                                    |        |              |
                                    +--------+  quagga4     |
                                             |     4.4.4.4  |
                                             +--------------+
````

### OpendayLight

The code structure is created using MAVEN archetypes.
The service Yang model is placed in API/../src folder.
During the building process 'yangtools' generate the implementation folder with provider model in it.
The Java code implemented the logic of the services defined in Yang.
Please refer to Opendaylight website for more information on building and deploying projects.
Also make sure Maven is installed with correct config for the version of OpenDaylight you want to install.


* link:
<https://docs.opendaylight.org/en/stable-fluorine/developer-guide/developing-apps-on-the-opendaylight-controller.html>
### Python_work
It includes the following scripts:
> A - For the sake of comparing OpenDaylight with Napalm, similar functionality was implemented in Napalm. The code base is smaller, however additional services such as restconf needs to be built on top.

> B - Simple ODL client has been created for controlling the ODL MDSAL model.

> C - rate monitoring script in the same folder was used to provide a means of implementing dynamic decision making based on some thresholds. The numbers used are arbitrary and for demo purposes only.

There is a __main__ file provided in the python_work folder for ease of testing.

The code has been tested with Python 2.7.13, and used under Pycharm . Running it as a library or module might need modification on the system and path.
