'''
test napalm connection with nodes'''
import json

from napalm import get_network_driver

driver_policy = get_network_driver('iosxr')
driver_monitor = get_network_driver('iosxr')

dev_policy = driver_policy(hostname='1.1.1.1', username='cisco', password='cisco')

dev_monitor = driver_monitor(hostname='2.2.2.2', username='cisco', password='cisco')

dev_policy.open()
dev_monitor.open()
bgp_neighbors = dev_policy.get_bgp_neighbors()
interface_counters = dev_policy.get_interfaces_counters()


dev_policy.close()
dev_monitor.close()
print(json.dumps(bgp_neighbors, sort_keys=True, indent=4))
print(json.dumps(interface_counters, sort_keys=True, indent=4))
