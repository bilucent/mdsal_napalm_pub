'''
Author          : Bijan R.Rofoee
'''
import os
import json
import requests
import time


class ODLMonitor():
    '''
    polling interface counters through CONFY API.
    '''

    def __init__(self, conf):
        self.odl_ip = conf["ODL_IP"]
        self.odl_port = conf["ODL_port"]
        self.policy_node = conf["node1"]["name"]
        self.monitor_node = conf["node2"]["name"]
        self.odl_user = conf["ODL_user"]
        self.odl_pass = conf["ODL_pass"]
        self.req_hdrs = {'Content-Type': 'application/json'}
        self.interface1 = conf["intf1"]
        self.interface2 = conf["intf2"]
        self.test_odl()

    def get_intf_util(self):
        url = 'http://' + self.odl_ip + ':' + self.odl_port + '/restconf/operations/confy:interface-util'
        request_template = """
        { 
            "input": {    
                "node-name": %s,    
                "interface-name": "eth0"    
            }
        }
        """
        req_body = request_template % (self.monitor_node)
        resp = requests.post(url, data=req_body, headers=self.req_hdrs, auth=(self.odl_user, self.odl_pass))
        return resp.content

    # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    # the three functions below demonstrate how different services are decoupled.
    # the logic basically is a subs traction.
    # then there is the function which fetched the utilisation per interface.
    def simple_rate_cal(self, interface):
        # interface = self.interface2

        # print self.simple_fetch_util(self.interface1)
        data1 = float(self.simple_fetch_util(interface))
        time.sleep(1)  # delays for 5 seconds
        data2 = float(self.simple_fetch_util(interface))
        return data2 - data1

    # interface util fetcher
    def simple_fetch_util(self, interface):
        resp = self.get_intf_util()
        results = json.loads(resp)
        return self.json_parse_util(results, interface)

    # interface util parser to get the right information for specifc interface
    def json_parse_util(self, results, interface):
        for o in results["output"]["ifo"]:
            ifos = o
            if ifos['interface-name'] == interface:
                return ifos['total-frames-transmitted']

    def test_odl(self):
        req_hdrs = {'Content-Type': 'application/xml'}

        url = 'http://' + self.odl_ip + ':' + self.odl_port + '/restconf/modules'
        try:
            resp = requests.get(url, headers=req_hdrs, auth=(self.odl_user, self.odl_pass))
            return resp.content, resp.status_code
        except:
            print("ODL not connected")
            exit(1)
