'''
Author          : Bijan R.Rofoee
'''

import logging
import json
import shutil
import time
import os
from napalm import get_network_driver


class NapalmDriver():
    def __init__(self,  devicetype, **conf):
        self.driver = get_network_driver(devicetype)
        self.dev_policy = self.driver(hostname=conf["node1"]["name"],
                                      username=conf["napalm_user"], password=conf["napalm_pass"])
        self.dev_policy.open()
        self.dev_mon = self.driver(hostname=conf["node2"]["name"],
                                   username=conf["napalm_user"], password=conf["napalm_pass"])
        self.dev_mon.open()
        self.node1 = conf["node1"]["ip"]
        self.node2 = conf["node2"]["ip"]
        self.self.external_host = "55.55.55.55"

    # //////////atomic functions
    def push_acl(self, acl_name):
        orig_file = 'cisco_config_files/acl.cfg'
        temp_file = 'cisco_config_files/acl.tmp'
        cp_files_tmp(orig_file, temp_file)
        with open(temp_file, 'r+') as conffile:
            file_data = conffile.read()
            file_data = file_data.replace('NAME', acl_name)
        with open(temp_file, 'w') as conffile:
            conffile.write(file_data)
        self.dev_policy.load_merge_candidate(filename=temp_file)
        self.dev_policy.commit_config()
        os.remove(temp_file)

    def del_acl(self, acl_name):
        orig_file = 'cisco_config_files/acl_del.cfg'
        temp_file = 'cisco_config_files/acl_del.tmp'
        cp_files_tmp(orig_file, temp_file)
        with open(temp_file, 'r+') as conffile:
            file_data = conffile.read()
            file_data = file_data.replace('NAME', acl_name)
        with open(temp_file, 'w') as conffile:
            conffile.write(file_data)
        self.dev_policy.load_merge_candidate(filename=temp_file)
        self.dev_policy.commit_config()
        os.remove(temp_file)

    def push_static_route(self, host, nh, metric):
        orig_file = 'cisco_config_files/static_route.cfg'
        temp_file = 'cisco_config_files/static_route.tmp'
        cp_files_tmp(orig_file, temp_file)
        with open(temp_file, 'r+') as conffile:
            file_data = conffile.read()
            file_data = file_data.replace('HOST', host)
            file_data = file_data.replace('GW', nh)
            file_data = file_data.replace('LMETR', str(metric))
        with open(temp_file, 'w') as conffile:
            conffile.write(file_data)
        self.dev_policy.load_merge_candidate(filename=temp_file)
        self.dev_policy.commit_config()
        os.remove(temp_file)

    def del_static_route_full(self, host, nh):
        orig_file = 'cisco_config_files/static_route_del_fullpath.cfg'
        temp_file = 'cisco_config_files/static_route_del_fullpath.tmp'
        cp_files_tmp(orig_file, temp_file)
        with open(temp_file, 'r+') as conffile:
            file_data = conffile.read()
            file_data = file_data.replace('HOST', host)
            file_data = file_data.replace('GW', nh)
        with open(temp_file, 'w') as conffile:
            conffile.write(file_data)
        self.dev_policy.load_merge_candidate(filename=temp_file)
        self.dev_policy.commit_config()
        os.remove(temp_file)

    def del_static_route(self, host):
        orig_file = 'cisco_config_files/static_route_del.cfg'
        temp_file = 'cisco_config_files/static_route_del.tmp'
        cp_files_tmp(orig_file, temp_file)
        with open(temp_file, 'r+') as conffile:
            file_data = conffile.read()
            file_data = file_data.replace('HOST', host)
        with open(temp_file, 'w') as conffile:
            conffile.write(file_data)
        self.dev_policy.load_merge_candidate(filename=temp_file)
        self.dev_policy.commit_config()
        os.remove(temp_file)

    # //////////services
    def left(self, metric, del_routes_first):
        if (del_routes_first):
            self.del_static_route(self.external_host)
        print("metric is ", metric)
        self.push_static_route(self.external_host, "tunnel-te11", metric)
        self.del_static_route_full(self.external_host, "tunnel-te12")

    def right(self, metric, del_routes_first):
        if (del_routes_first):
            self.del_static_route(self.external_host)
        self.push_static_route(self.external_host, "tunnel-te12", metric)
        self.del_static_route_full(self.external_host, "tunnel-te11")

    def loadbalance(self, l_metric, r_metric):
        # self.del_static_route(self.external_host)
        self.push_static_route(self.external_host, "tunnel-te11", l_metric)
        self.push_static_route(self.external_host, "tunnel-te12", r_metric)

    def drop(self):
        self.del_static_route(self.external_host)

    # moniotring/samplig code is here

    # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    # the three functions below demonstrate how different services are decoupled.
    # the logic basicaly is a substraction.
    # then there is the funtion which fetched the utilisation per interface.
    def simple_rate_cal(self, dev_mon, interface):
        data1 = float(self.simple_fetch_util(dev_mon, interface))
        time.sleep(1)  # delays for 5 seconds
        data2 = float(self.simple_fetch_util(dev_mon, interface))
        return data2 - data1

    def simple_fetch_util(self, dev_mon, interface):
        resp = dev_mon.get_interfaces_counters()
        return resp[interface]["rx_unicast_packets"]

    # interface util parser to get the right information for specifc interface
    # def dict_parse_util(results, interface):
    #     pass

    def te_logic(self, rate):
        print("rate ", rate)
        if rate <= 4:
            return "DROP", 0, 0
        elif rate <= 15:
            return "LEFT", 10, 0
        elif rate > 15 and rate <= 25:
            return "LB", 10, 10
        elif rate > 25:
            return "LB", 10, 20
        else:
            pass

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    def right_left(self, policy_name, policy_action, metric, metric2):
        if policy_action is "LEFT":
            self.left(metric, False)
        elif policy_action is "RIGHT":
            self.right(metric, False)
        elif policy_action is "LB":
            self.loadbalance(metric, metric2)
        elif policy_action is "DROP":
            self.drop()
        else:
            logging.error("XXXXXXXXXXXXXXXXXX action error XXXXXXXXXXXXXXXXXX")


def cp_files_tmp(src,  file_name):
    full_file_name = os.path.join(src)
    full_destination = os.path.join(file_name)
    if (os.path.isfile(full_file_name)):
        while os.path.exists(full_destination):
            full_destination += ".duplicate"
        shutil.copy(full_file_name, full_destination)
