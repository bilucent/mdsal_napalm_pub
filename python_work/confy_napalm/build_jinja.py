'''
Author          : Bijan R.Rofoee
'''
from jinja2 import Environment, FileSystemLoader, PackageLoader
from napalm_base import get_network_driver

node1 = "1.1.1.1"
node2 = "2.2.2.2"
external_host = "55.55.55.55/32"


def push_acl():
    driver = get_network_driver('iosxr')
    dev = driver(hostname=node1, username='cisco', password='cisco')
    dev.open()
    dev.load_merge_candidate(filename='acl.cfg')
    dev.commit_config()
    dev.close()


def del_acl():
    driver = get_network_driver('iosxr')
    dev = driver(hostname=node1, username='cisco', password='cisco')
    dev.open()
    dev.load_merge_candidate(filename='acl_del.cfg')
    dev.commit_config()
    dev.close()


def push_static_route(env):
    driver = get_network_driver('iosxr')
    dev = driver(hostname=node1, username='cisco', password='cisco')
    dev.open()
    commands = str(env.get_template("static_route_del.j2").render(ipv4_and_mask=external_host))
    comlist = commands.splitlines()

    dev.cli(comlist)

    dev.commit_config()
    dev.close()


def del_static_route():
    driver = get_network_driver('iosxr')
    dev = driver(hostname=node1, username='cisco', password='cisco')
    dev.open()
    dev.load_merge_candidate(filename='static_route_del.cfg')
    dev.commit_config()
    dev.close()


# if __name__ == '__main__':
#     env = Environment(loader=PackageLoader('push_config', 'templates'))
#     # env = Environment(loader=FileSystemLoader('templates'), trim_blocks=True)
#     # template = env.get_template("static_route.j2")
#     # print(template.render(ipv4_and_mask='55.55.55.55/32'))
#     # driver = get_network_driver('iosxr')
#     # dev = driver(hostname='1.1.1.1', username='cisco', password='cisco')
#     # dev.open()
#     # dev.load_merge_candidate(filename='static_route_del.cfg')
#     # dev.commit_config()
#     # dev.close()
#     # del_acl()
#     push_static_route(env)
